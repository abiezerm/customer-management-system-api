package com.customer_management_system.app.configuration;

import com.customer_management_system.app.util.Constants;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;


/**
 * swagger configuration.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Docket bean to configure springfox-swagger2.
     *
     * @return Docket configuration
     */
    @Bean
    public Docket api () {
        return new Docket (DocumentationType.SWAGGER_2)
                .select ()
                .apis (RequestHandlerSelectors.any ())
                .paths (PathSelectors.any ())
                .build ()
                .apiInfo (apiInfo ());
    }

    /**
     * Api info.
     *
     * @return the api info
     */
    @SuppressWarnings (Constants.RAWTYPES)
    private ApiInfo apiInfo () {

        return new ApiInfo (Constants.API_DOCUMENTATION_LITERAL, Constants.API_REST_LITERAL, Constants.API_VERSION_LITERAL, Constants.EMPTY,
                new Contact(Constants.CHALLENGE_TEAM, Constants.EMPTY, Constants.EMAIL_LITERAL), Constants.EMPTY, Constants.EMPTY, new ArrayList<VendorExtension>());
    }


}