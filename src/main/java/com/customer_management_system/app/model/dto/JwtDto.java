package com.customer_management_system.app.model.dto;


import org.springframework.boot.SpringApplication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;



/**
 * JwtDto class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
public class JwtDto extends DataDto {

    /**
     * token
     */
    private String token;

    /**
     * bearer
     */
    private String bearer = "Bearer";

    /**
     * userName
     */
    private String userName;

    /**
     * GrantedAuthority Collection
     */
    private Collection<? extends GrantedAuthority> authorities;

    /**
     * param Constructor
     *
     * @param token
     * @param userName
     * @param authorities
     */
    public JwtDto(String token, String userName, Collection<? extends GrantedAuthority> authorities) {
        this.token = token;
        this.userName = userName;
        this.authorities = authorities;
    }

    /**
     * getAuthorities
     * @return  Collection GrantedAuthority
     */
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     *  set Authorities
     * @param authorities
     */
    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    /**
     * Sets new bearer.
     *
     * @param bearer New value of bearer.
     */
    public void setBearer(String bearer) {
        this.bearer = bearer;
    }

    /**
     * Gets bearer.
     *
     * @return Value of bearer.
     */
    public String getBearer() {
        return bearer;
    }

    /**
     * Gets userName.
     *
     * @return Value of userName.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets new token.
     *
     * @param token New value of token.
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Gets token.
     *
     * @return Value of token.
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets new userName.
     *
     * @param userName New value of userName.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
}