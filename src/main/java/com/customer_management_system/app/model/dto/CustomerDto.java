package com.customer_management_system.app.model.dto;

import com.customer_management_system.app.util.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.boot.SpringApplication;

import java.time.ZonedDateTime;
import java.util.List;


/**
 *  ItemDto class
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since MArch 24, 2021
 * @see SpringApplication
 */
public class CustomerDto extends DataDto {


    private String customerId;

    /**
     * the first name of the customer
     */

    private String firstName;

    /**
     * the last name of the customer
     */
    private String lastName;

    /**
     * customer Social security
     */

    private String socialSecurity;

    /**
     * the timestamp at which the customer is saved in the database
     */
    @JsonFormat(pattern = Constants.DATE_HOUR_PATTERN)
    private ZonedDateTime customerEnteredDate;

    /**
     * the status of the customer
     */
    private String customerStatus;


    /**
     * customers address
     */
    private List<AddressDto> customerAddress;


    /**
     * Gets the first name of the customer.
     *
     * @return Value of the first name of the customer.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets new customers address.
     *
     * @param customerAddress New value of customers address.
     */
    public void setCustomerAddress(List<AddressDto> customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     * Sets new the first name of the customer.
     *
     * @param firstName New value of the first name of the customer.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Sets new customerId.
     *
     * @param customerId New value of customerId.
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets customerId.
     *
     * @return Value of customerId.
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Gets the status of the customer.
     *
     * @return Value of the status of the customer.
     */
    public String getCustomerStatus() {
        return customerStatus;
    }

    /**
     * Gets customer Social security.
     *
     * @return Value of customer Social security.
     */
    public String getSocialSecurity() {
        return socialSecurity;
    }

    /**
     * Sets new the status of the customer.
     *
     * @param customerStatus New value of the status of the customer.
     */
    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    /**
     * Sets new the last name of the customer.
     *
     * @param lastName New value of the last name of the customer.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Sets new the timestamp at which the customer is saved in the database.
     *
     * @param customerEnteredDate New value of the timestamp at which the customer is saved in the database.
     */
    public void setCustomerEnteredDate(ZonedDateTime customerEnteredDate) {
        this.customerEnteredDate = customerEnteredDate;
    }

    /**
     * Gets customers address.
     *
     * @return Value of customers address.
     */
    public List<AddressDto> getCustomerAddress() {
        return customerAddress;
    }

    /**
     * Gets the last name of the customer.
     *
     * @return Value of the last name of the customer.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets new customer Social security.
     *
     * @param socialSecurity New value of customer Social security.
     */
    public void setSocialSecurity(String socialSecurity) {
        this.socialSecurity = socialSecurity;
    }

    /**
     * Gets the timestamp at which the customer is saved in the database.
     *
     * @return Value of the timestamp at which the customer is saved in the database.
     */
    public ZonedDateTime getCustomerEnteredDate() {
        return customerEnteredDate;
    }
}
