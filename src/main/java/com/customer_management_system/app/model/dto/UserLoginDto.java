package com.customer_management_system.app.model.dto;

import org.springframework.boot.SpringApplication;

import javax.validation.constraints.NotBlank;


/**
 * ServiceResponseDto class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
public class UserLoginDto {


    /**
     * userName
     */
    @NotBlank
    private String userName;

    /**
     * password
     */
    @NotBlank
    private String password;


    /**
     * Sets new password.
     *
     * @param password New value of password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets password.
     *
     * @return Value of password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets new userName.
     *
     * @param userName New value of userName.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets userName.
     *
     * @return Value of userName.
     */
    public String getUserName() {
        return userName;
    }
}