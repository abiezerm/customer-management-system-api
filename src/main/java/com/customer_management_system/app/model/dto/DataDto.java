package com.customer_management_system.app.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.boot.SpringApplication;


/**
 * DataDto class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
public abstract class DataDto {


    /**
     * The type.
     */
    @JsonIgnore
    private String type;

    /**
     * Customized constructor
     *
     * @param strType the str type
     */
    protected DataDto(String strType) {

        this.type = strType;
    }

    /**
     * Default constructor
     */
    protected DataDto() {
        this.type = this.getClass().getSimpleName();
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Sets the type.
     *
     * @param strType the new type
     */
    public void setType(String strType) {
        this.type = strType;
    }

}
