package com.customer_management_system.app.model.dto;


import org.springframework.boot.SpringApplication;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

/**
 * RequestUserDto class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
public class RequestUserDto {

    /**
     * companyName
     */
    @NotBlank
    private String companyName;

    /**
     * username
     */
    @NotBlank
    private String username;

    /**
     * email
     */
    @Email
    private String email;

    /**
     * password
     */
    @NotBlank
    private String password;

    /**
     * set of roles
     */
    private Set<String> roles = new HashSet<>();


    /**
     * Gets email.
     *
     * @return Value of email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets new firstName.
     *
     * @param companyName New value of firstName.
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Sets new email.
     *
     * @param email New value of email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets username.
     *
     * @return Value of username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets firstName.
     *
     * @return Value of firstName.
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Gets password.
     *
     * @return Value of password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets set of roles.
     *
     * @return Value of set of roles.
     */
    public Set<String> getRoles() {
        return roles;
    }

    /**
     * Sets new set of roles.
     *
     * @param roles New value of set of roles.
     */
    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    /**
     * Sets new password.
     *
     * @param password New value of password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Sets new username.
     *
     * @param username New value of username.
     */
    public void setUsername(String username) {
        this.username = username;
    }
}