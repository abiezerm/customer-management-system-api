package com.customer_management_system.app.model.dto;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.boot.SpringApplication;

/**
 * ErrorDto class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class ErrorDto {

    /**
     * status
     */
    private String status;

    /**
     * code
     */
    private String code;

    /**
     * title
     */
    private String title;

    /**
     * detail
     */
    private String detail;

    /**
     * resource
     */
    private String resource;

    /**
     * Default Constructor
     */
    public ErrorDto() {
    }

    /**
     * Param Constructor
     *
     * @param strStatus
     * @param strCode
     * @param strTitle
     * @param strDetail
     * @param strResource
     */
    public ErrorDto(String strStatus, String strCode, String strTitle, String strDetail, String strResource) {
        this.status = strStatus;
        this.code = strCode;
        this.title = strTitle;
        this.detail = strDetail;
        this.resource = strResource;
    }


    /**
     * Gets status.
     *
     * @return Value of status.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Gets resource.
     *
     * @return Value of resource.
     */
    public String getResource() {
        return resource;
    }

    /**
     * Gets detail.
     *
     * @return Value of detail.
     */
    public String getDetail() {
        return detail;
    }

    /**
     * Gets code.
     *
     * @return Value of code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets new title.
     *
     * @param title New value of title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Sets new status.
     *
     * @param status New value of status.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets title.
     *
     * @return Value of title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets new code.
     *
     * @param code New value of code.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Sets new detail.
     *
     * @param detail New value of detail.
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * Sets new resource.
     *
     * @param resource New value of resource.
     */
    public void setResource(String resource) {
        this.resource = resource;
    }
}