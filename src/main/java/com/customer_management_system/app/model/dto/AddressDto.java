package com.customer_management_system.app.model.dto;

import org.springframework.boot.SpringApplication;

/**
 * Customer Entity mapping.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
public class AddressDto {


    /**
     * address Id
     */
    private int addressId;

    /**
     * street name
     */
    private String street;


    /**
     * apartment number
     */
    private String apartment;

    /**
     * state name
     */
    private String state;

    /**
     * city name
     */
    private String city;

    /**
     * zip code number
     */
    private Integer zipCode;


    /**
     * country name
     */
    private String country;



    /**
     * Sets new state name.
     *
     * @param state New value of state name.
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Sets new apartment number.
     *
     * @param apartment New value of apartment number.
     */
    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    /**
     * Sets new address Id.
     *
     * @param addressId New value of address Id.
     */
    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    /**
     * Sets new country name.
     *
     * @param country New value of country name.
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets city name.
     *
     * @return Value of city name.
     */
    public String getCity() {
        return city;
    }

    /**
     * Gets country name.
     *
     * @return Value of country name.
     */
    public String getCountry() {
        return country;
    }

    /**
     * Gets state name.
     *
     * @return Value of state name.
     */
    public String getState() {
        return state;
    }

    /**
     * Gets apartment number.
     *
     * @return Value of apartment number.
     */
    public String getApartment() {
        return apartment;
    }

    /**
     * Sets new street name.
     *
     * @param street New value of street name.
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Sets new zip code number.
     *
     * @param zipCode New value of zip code number.
     */
    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * Sets new city name.
     *
     * @param city New value of city name.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets street name.
     *
     * @return Value of street name.
     */
    public String getStreet() {
        return street;
    }

    /**
     * Gets zip code number.
     *
     * @return Value of zip code number.
     */
    public Integer getZipCode() {
        return zipCode;
    }

    /**
     * Gets address Id.
     *
     * @return Value of address Id.
     */
    public int getAddressId() {
        return addressId;
    }
}
