package com.customer_management_system.app.model.enums;

import org.springframework.boot.SpringApplication;

/**
 * ItemStatus enum.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public enum RoleName {

    ROLE_ADMIN, ROLE_USER
}