package com.customer_management_system.app.security.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *  JwtEntryPoint
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Component
public class JwtEntryPoint implements AuthenticationEntryPoint {

    /**
     * Constants logger
     */
    private final static Logger logger = LoggerFactory.getLogger(JwtEntryPoint.class);

    /**
     * commence (AuthenticationEntryPoint)
     * @param request
     * @param response
     * @param authException
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {
        logger.error("method commence fail");
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Not Authorize");
    }
}