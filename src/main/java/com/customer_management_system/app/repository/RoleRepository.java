package com.customer_management_system.app.repository;


import com.customer_management_system.app.entity.Role;
import com.customer_management_system.app.model.enums.RoleName;
import org.springframework.boot.SpringApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Role Repository.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    /**
     *  findByRoleName
     * @param rolName
     * @return
     */
    Optional<Role> findByRoleName(RoleName rolName);
}