package com.customer_management_system.app.exeption;

import org.springframework.boot.SpringApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;

import java.io.Serializable;

/**
 * HandlerException class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public class HandlerException implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3241577270295092439L;

    /**
     * title
     */
    private String title;

    /**
     * detail
     */
    private String detail;

    /**
     * httpStatus
     */
    private HttpStatus httpStatus;

    /**
     * webRequest
     */
    private WebRequest webRequest;

    /**
     * exception
     */
    private Exception exception;

    /**
     * Constructor
     */
    public HandlerException() {
    }

    /**
     *
     * param Constructor
     *
     * @param title
     * @param detail
     * @param httpStatus
     * @param webRequest
     * @param exception
     */
    public HandlerException(String title, String detail, HttpStatus httpStatus, WebRequest webRequest, Exception exception) {
        this.title = title;
        this.detail = detail;
        this.httpStatus = httpStatus;
        this.webRequest = webRequest;
        this.exception = exception;
    }


    /**
     * Gets webRequest.
     *
     * @return Value of webRequest.
     */
    public WebRequest getWebRequest() {
        return webRequest;
    }

    /**
     * Sets new webRequest.
     *
     * @param webRequest New value of webRequest.
     */
    public void setWebRequest(WebRequest webRequest) {
        this.webRequest = webRequest;
    }

    /**
     * Gets title.
     *
     * @return Value of title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets serialVersionUID.
     *
     * @return Value of serialVersionUID.
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * Gets detail.
     *
     * @return Value of detail.
     */
    public String getDetail() {
        return detail;
    }

    /**
     * Sets new detail.
     *
     * @param detail New value of detail.
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * Sets new title.
     *
     * @param title New value of title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets httpStatus.
     *
     * @return Value of httpStatus.
     */
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    /**
     * Gets exception.
     *
     * @return Value of exception.
     */
    public Exception getException() {
        return exception;
    }

    /**
     * Sets new exception.
     *
     * @param exception New value of exception.
     */
    public void setException(Exception exception) {
        this.exception = exception;
    }

    /**
     * Sets new httpStatus.
     *
     * @param httpStatus New value of httpStatus.
     */
    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}