package com.customer_management_system.app.exeption;

import org.springframework.boot.SpringApplication;

import java.util.HashMap;
import java.util.Map;

/**
 * BaseSearchException Class .
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public class BaseSearchException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private Map<String, String> searchParams;
    private String searchName;

    /**
     * Constructor
     */
    public BaseSearchException() {
    }

    /**
     *  getSearchParams
     * @return
     */
    public Map<String, String> getSearchParams() {
        Map<String, String> immutableMap = this.searchParams != null ? new HashMap(this.searchParams) : null;
        return immutableMap;
    }

    /**
     * setSearchParams
     * @param searchParams
     */
    public void setSearchParams(Map<String, String> searchParams) {
        Map<String, String> immutableMap = searchParams != null ? new HashMap(searchParams) : null;
        this.searchParams = immutableMap;
    }

    /**
     * searchParamsAsString
     * @return
     */
    public String searchParamsAsString() {
        if (this.searchParams != null && !this.searchParams.isEmpty()) {
            String string2Return = this.searchParams.toString();
            return string2Return.substring(1, string2Return.length() - 1);
        } else {
            return null;
        }
    }

    /**
     * getSearchName
     * @return
     */
    public String getSearchName() {
        return this.searchName;
    }

    /**
     *  setSearchName
     * @param searchName
     */
    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}