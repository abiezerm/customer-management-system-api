package com.customer_management_system.app.exeption;

import com.customer_management_system.app.util.CommonMessages;
import com.customer_management_system.app.util.Constants;
import com.customer_management_system.app.model.dto.CommonResponseDto;
import com.customer_management_system.app.model.dto.ErrorDto;
import com.customer_management_system.app.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.io.IOException;


/**
 * RestResponseEntityExceptionHandler class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 25, 2021
 * @see SpringApplication
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    /**
     * Constructor
     */
    public RestResponseEntityExceptionHandler() {
    }

    /**
     * ExceptionHandler for Generic Exceptions
     * @param exception
     * @param webRequest
     * @return
     */
    @ExceptionHandler({Exception.class})
    protected ResponseEntity<CommonResponseDto> handleExceptions(Exception exception, WebRequest webRequest) {
        HttpStatus responseStatus = HttpStatus.CONFLICT;
        String detail = exception.getClass().getSimpleName();
        byte var5 = -1;
        switch (detail.hashCode()) {
            case -597987214:
                if (detail.equals("BindException")) {
                    var5 = 1;
                }
                break;
            case -584950079:
                if (detail.equals("NestedServletException")) {
                    var5 = 0;
                }
                break;
            case 816101239:
                if (detail.equals("RuntimeException")) {
                    var5 = 2;
                }
                break;
            case 1832984524:
                if (detail.equals("HttpRequestMethodNotSupportedException")) {
                    var5 = 3;
                }
        }

        switch (var5) {
            case 0:
            case 1:
                responseStatus = HttpStatus.BAD_REQUEST;
                break;
            case 2:
                responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                break;
            case 3:
                responseStatus = HttpStatus.METHOD_NOT_ALLOWED;
        }

        String var10000 = exception.getClass().getName();
        detail = var10000 + " " + exception.getMessage();
        return Util.buildHandlerExceptionResponse(new HandlerException(responseStatus.getReasonPhrase(), detail, responseStatus, webRequest, exception));
    }

    /**
     *  ExceptionHandler for MethodArgumentNotValidException
     * @param exception
     * @param webRequest
     * @return
     * @throws IOException
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    protected ResponseEntity<CommonResponseDto> handleMethodNotValid(MethodArgumentNotValidException exception, WebRequest webRequest) throws IOException {
        Logger logger = LOGGER;
        logger.error(exception.getMessage(), exception);
        CommonResponseDto response = (new CommonMessages()).createArgumentErrorObject(exception, webRequest);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.DATE_HEADER, new String[]{Util.getActualZoneDateTime()}).body(response);
    }

    /**
     * ExceptionHandler for IllegalArgumentException
     * @param exception
     * @param webRequest
     * @return
     */
    @ExceptionHandler({IllegalArgumentException.class})
    protected ResponseEntity<CommonResponseDto> handleIllegalArgument(Exception exception, WebRequest webRequest) {
        Logger logger = LOGGER;
        logger.error(exception.getMessage(), exception);
        CommonResponseDto response = (new CommonMessages()).createIllegalArgumentErrorObject(exception, webRequest);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.DATE_HEADER, new String[]{Util.getActualZoneDateTime()}).body(response);
    }

    /**
     * ExceptionHandler for BadRequestException
     * @param exception
     * @param webRequest
     * @return
     */
    @ExceptionHandler({BadRequestException.class})
    protected ResponseEntity<CommonResponseDto> handleBadRequest(BadRequestException exception, WebRequest webRequest) {
        Logger logger = LOGGER;
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        logger.error(exception.getMessage(), exception);
        ErrorDto errorDto = Util.createErrorDto(HttpStatus.BAD_REQUEST, webRequest);
        errorDto.setTitle(exception.getTitle());
        errorDto.setDetail(exception.getMessage());
        commonResponseDto.addError(errorDto);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Constants.DATE_HEADER, Util.getActualZoneDateTime()).body(commonResponseDto);
    }


    /**
     * ExceptionHandler for AccessDeniedException
     * @param exception
     * @param webRequest
     * @return
     */
    @ExceptionHandler({AccessDeniedException.class})
    protected ResponseEntity<CommonResponseDto> handleAccessDeniedException(AccessDeniedException exception, WebRequest webRequest) {
        Logger logger = LOGGER;
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        logger.error(exception.getMessage(), exception);
        ErrorDto errorDto = Util.createErrorDto(HttpStatus.FORBIDDEN, webRequest);
        errorDto.setTitle("Access Denied");
        errorDto.setDetail("Authorization Failure-This user does not have the sufficient level of access.");
        commonResponseDto.addError(errorDto);
        return ResponseEntity.status(HttpStatus.FORBIDDEN).header(Constants.DATE_HEADER, Util.getActualZoneDateTime()).body(commonResponseDto);
    }

    /**
     * ExceptionHandler for BadCredentialsException
     * @param exception
     * @param webRequest
     * @return
     */
    @ExceptionHandler({BadCredentialsException.class, InternalAuthenticationServiceException.class})
    protected ResponseEntity<CommonResponseDto> handleBadCredentialsException(BadCredentialsException exception, WebRequest webRequest) {
        Logger logger = LOGGER;
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        logger.error(exception.getMessage(), exception);
        ErrorDto errorDto = Util.createErrorDto(HttpStatus.UNAUTHORIZED, webRequest);
        errorDto.setTitle("Invalid Request");
        errorDto.setDetail("Authentication Failure-The user name and password combination is incorrect");
        commonResponseDto.addError(errorDto);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).header(Constants.DATE_HEADER, Util.getActualZoneDateTime()).body(commonResponseDto);
    }


    /**
     * ExceptionHandler for RecordNotFoundException
     * @param exception
     * @param webRequest
     * @return
     */
    @ExceptionHandler({RecordNotFoundException.class})
    protected ResponseEntity<CommonResponseDto> handleRecordNotFoundException(RecordNotFoundException exception, WebRequest webRequest) throws IOException {
        Logger logger = LOGGER;
        logger.error(exception.getMessage(), exception);
        CommonResponseDto response = (new CommonMessages()).createBaseSearchException(exception, webRequest);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).header(Constants.DATE_HEADER, new String[]{Util.getActualZoneDateTime()}).body(response);
    }


    /**
     * ExceptionHandler for HttpRequestMethodNotSupportedException
     * @param exception
     * @param webRequest
     * @return
     */
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    protected ResponseEntity<CommonResponseDto> handleHttpRequestMethodNotSupportedException(Exception exception, WebRequest webRequest) {
        return Util.buildHandlerExceptionResponse(new HandlerException("Method Not Allowed", exception.getMessage(), HttpStatus.METHOD_NOT_ALLOWED, webRequest, exception));
    }


    /**
     * ExceptionHandler for NoHandlerFoundException
     * @param exception
     * @param webRequest
     * @return
     */
    @ExceptionHandler({NoHandlerFoundException.class})
    protected ResponseEntity<CommonResponseDto> handleNoHandlerFoundException(NoHandlerFoundException exception, WebRequest webRequest) {
        return Util.buildHandlerExceptionResponse(new HandlerException("Resource Not found", "The resource were not found.", HttpStatus.NOT_FOUND, webRequest, exception));
    }




}