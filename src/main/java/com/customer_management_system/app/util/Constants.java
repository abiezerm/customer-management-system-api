package com.customer_management_system.app.util;

import org.springframework.boot.SpringApplication;

/**
 * Constants .
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public interface Constants {


    /**
     * Url customers endpoints
     */
    String ENDPOINT_CUSTOMERS = "/customers";


    /**
     * Url to customer catalog
     */
    String ENDPOINT_ITEMS_PAGE = "/customers-catalog";


    /**
     * ENDPOINT_SIGN_ON
     */
    String ENDPOINT_SIGN_ON = "/sign-on";


    String ENDPOINT_LOGIN = "/login";


    /**
     * URL_CALL_MESSAGE
     */
    String URL_CALL_MESSAGE = "{} Url is being called";

    /**
     * The constant API_DOCUMENTATION_LITERAL.
     */
    String API_DOCUMENTATION_LITERAL = "Api Documentation";

    /**
     * The Constant RAWTYPES.
     */
    String RAWTYPES = "rawtypes";

    /**
     * The constant API_REST_LITERAL.
     */
    String API_REST_LITERAL = "REST";

    /**
     * The constant API_VERSION_LITERAL.
     */
    String API_VERSION_LITERAL = "v1.0";

    /**
     * The constant CHALLENGE_TEAM
     */
    String CHALLENGE_TEAM = "New Inventory Team";

    /**
     * The constant EMAIL_LITERAL
     */
    String EMAIL_LITERAL = "RichieZaul@gmail.com";

    /**
     * The constant EMPTY
     */
    String EMPTY = "";


    /**
     * The date hour pattern.
     */
    String DATE_HOUR_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS";

    /**
     * PATH
     */
    String PATH = "/v1";

    /**
     * DATE_TITLE
     */
    String DATE_HEADER = "Date";

}
