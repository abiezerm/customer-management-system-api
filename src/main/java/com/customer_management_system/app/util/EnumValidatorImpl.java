package com.customer_management_system.app.util;

import org.springframework.boot.SpringApplication;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
/**
 * EnumValidatorImpl class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since January 22, 2021
 * @see SpringApplication
 */
public class EnumValidatorImpl implements ConstraintValidator<EnumValidator, String> {
    List<String> valueList = null;

    /**
     * Default Constructor
     */
    public EnumValidatorImpl() {
    }

    /**
     * isValid
     * @param value
     * @param context
     * @return
     */
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null ? true : this.valueList.contains(value);
    }

    /**
     * initialize
     * @param constraintAnnotation
     */
    public void initialize(EnumValidator constraintAnnotation) {
        this.valueList = new ArrayList();
        Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClazz();
        if (enumClass != null) {
            Enum[] enumValArr = (Enum[])enumClass.getEnumConstants();
            Enum[] var4 = enumValArr;
            int var5 = enumValArr.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                Enum enumVal = var4[var6];
                this.valueList.add(enumVal.name());
            }
        }

    }
}