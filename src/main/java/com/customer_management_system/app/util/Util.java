package com.customer_management_system.app.util;

import com.customer_management_system.app.exeption.HandlerException;
import com.customer_management_system.app.model.dto.ErrorDto;
import com.customer_management_system.app.model.dto.CommonResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.context.request.WebRequest;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;



/**
 * Constants
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since January 22, 2021
 * @see SpringApplication
 */
public class Util {

    /**
     *
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);


    /**
     * Builds  responses Date header
     *
     * @param timezone the timezone
     * @return the utc date header
     */
    public static String getUTCDateHeader(String timezone) {

        DateFormat formatter = new SimpleDateFormat(Constants.DATE_HOUR_PATTERN);
        formatter.setTimeZone(TimeZone.getTimeZone(timezone));

        return formatter.format(new Date(System.currentTimeMillis()));

    }


    /**
     * createErrorDto
     * @param httpStatus
     * @param webRequest
     * @return
     */
    public static ErrorDto createErrorDto(HttpStatus httpStatus, WebRequest webRequest) {
        ErrorDto errorDto = new ErrorDto();
        if (webRequest != null) {
            errorDto.setResource(webRequest.getDescription(false));
        }

        errorDto.setStatus(httpStatus.name());
        errorDto.setCode(String.valueOf(httpStatus.value()));
        return errorDto;
    }

    /**
     * getActualZoneDateTime
     * @return String
     */
    public static String getActualZoneDateTime() {
        return ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    /**
     *  buildHandlerExceptionResponse
     * @param handlerException
     * @return
     */
    public static ResponseEntity<CommonResponseDto> buildHandlerExceptionResponse(HandlerException handlerException) {
        Logger logger = LOGGER;
        CommonResponseDto response = new CommonResponseDto();
        ErrorDto errorDto = createErrorDto(handlerException.getHttpStatus(), handlerException.getWebRequest());
        errorDto.setTitle(handlerException.getTitle());
        errorDto.setDetail(handlerException.getDetail());
        response.addError(errorDto);
        logger.error(handlerException.getException().getMessage(), handlerException.getException());
        return ((ResponseEntity.BodyBuilder)ResponseEntity.status(handlerException.getHttpStatus()).header("Date", new String[]{getActualZoneDateTime()})).body(response);
    }


    /**
     * validSortByparam
     * @param sort
     * @param object
     * @param <T>
     */
    public static <T> void validSortParam(String sort, Class <T> object){

        Assert.notNull(sort, "sort must not be null.");

            final List<Field> list = Arrays.stream(object.getDeclaredFields())
                    .filter(data -> data.getName().equalsIgnoreCase(sort)).collect(Collectors.toList());

            list.stream().findFirst().orElseThrow(() -> new IllegalArgumentException("Sort option is invalid"));

    }


}
