package com.customer_management_system.app.util;

import com.customer_management_system.app.exeption.BaseSearchException;
import com.customer_management_system.app.exeption.RecordNotFoundException;
import com.customer_management_system.app.model.dto.CommonResponseDto;
import com.customer_management_system.app.model.dto.ErrorDto;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;


/**
 *  JwtTokenFilter
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since January 23, 2021
 * @see SpringApplication
 */
@Component
@Configuration
public class CommonMessages {

    /**
     * Properties
     */
    private static Properties properties;

    /**
     * Default Constructor
     */
    public CommonMessages() {
    }

    /**
     * getLabelFromKey
     *
     * @param strKey
     * @param strField
     * @param strErrorCode
     * @param lstArgs
     * @return
     * @throws IOException
     */
    private String getLabelFromKey(String strKey, String strField, String strErrorCode, List<String> lstArgs) throws IOException {
        String message = "";
        if (properties == null) {
            properties = new Properties();
            properties.load(this.getClass().getResourceAsStream("/messages.properties"));
        }

        Properties propertiesLocal = properties;
        if (lstArgs != null && lstArgs.size() == 2 && (strErrorCode.equals("Length") || strErrorCode.equals("Size"))) {
            message = MessageFormat.format(propertiesLocal.getProperty(strKey.concat(".").concat(strErrorCode.toLowerCase())), strField, String.valueOf(lstArgs.get(0)), String.valueOf(lstArgs.get(1)));
        } else if (lstArgs != null && lstArgs.size() == 1 && !strErrorCode.equals("Length") && !strErrorCode.equals("Size")) {
            message = MessageFormat.format(propertiesLocal.getProperty(strKey.concat(".").concat(strErrorCode.toLowerCase())), strField, lstArgs.get(0));
        } else if (lstArgs == null || lstArgs.size() != 1 || !strErrorCode.equals("Length") && !strErrorCode.equals("Size")) {
            message = MessageFormat.format(propertiesLocal.getProperty(strKey.concat(".").concat(strErrorCode.toLowerCase())), strField);
        } else {
            String propertyName = strKey.concat(".").concat(strErrorCode).concat(".").concat("Min").concat(".").concat("Max").concat(".").concat("equals");
            message = MessageFormat.format(propertiesLocal.getProperty(propertyName.toLowerCase()), strField, String.valueOf(lstArgs.get(0)));
        }

        return message;
    }

    /**
     * getErrorBadRequest
     * @param fieldError
     * @param webRequest
     * @return
     * @throws IOException
     */
    private ErrorDto getErrorBadRequest(FieldError fieldError, WebRequest webRequest) throws IOException {
        Object[] argumentsArray = fieldError.getArguments();
        List<String> argumentsList = null;
        if (argumentsArray != null && argumentsArray.length > 2) {
            argumentsList = new ArrayList(2);
            String arg1 = String.valueOf(argumentsArray[1]);
            String arg2 = String.valueOf(argumentsArray[2]);
            if (("Length".equalsIgnoreCase(this.filterErrorCode(fieldError)) || "Size".equalsIgnoreCase(this.filterErrorCode(fieldError))) && Integer.parseInt(arg1) == Integer.parseInt(arg2)) {
                argumentsList.add(arg1);
            } else {
                argumentsList.add(arg2);
                argumentsList.add(arg1);
            }
        } else if (argumentsArray != null && argumentsArray.length > 1) {
            argumentsList = new ArrayList(1);
            argumentsList.add(String.valueOf(argumentsArray[1]));
        }

        ErrorDto error = new ErrorDto(HttpStatus.BAD_REQUEST.name(), String.valueOf(HttpStatus.BAD_REQUEST.value()), "Invalid data", this.getLabelFromKey("invalid.field", fieldError.getField(), this.filterErrorCode(fieldError), argumentsList), webRequest.getDescription(false));
        return error;
    }

    /**
     * filterErrorCode
     * @param fieldError
     * @return
     */
    public String filterErrorCode(FieldError fieldError) {
        String var2 = fieldError.getCode();
        byte var3 = -1;
        switch (var2.hashCode()) {
            case -501753126:
                if (var2.equals("NotNull")) {
                    var3 = 0;
                }
                break;
            case 1614161505:
                if (var2.equals("NotBlank")) {
                    var3 = 1;
                }
        }

        switch (var3) {
            case 0:
            case 1:
                if (fieldError.getRejectedValue() == null) {
                    return "NotNull";
                }

                return "NotBlank";
            default:
                return fieldError.getCode();
        }
    }

    /**
     * createBaseSearchException
     * @param exception
     * @param webRequest
     * @return
     * @throws IOException
     */
    public CommonResponseDto createBaseSearchException(BaseSearchException exception, WebRequest webRequest) throws IOException {
        CommonResponseDto response = new CommonResponseDto();
        String errorDetail = null;
        String errorTitle = null;
        HttpStatus status = null;
        String[] params = new String[2];
        if (exception instanceof RecordNotFoundException) {
            status = HttpStatus.NOT_FOUND;
            if (exception.getSearchParams() != null && !exception.getSearchParams().isEmpty()) {
                params[0] = exception.getSearchName();
                params[1] = exception.searchParamsAsString();
                errorDetail = "{0} was not found for {1}";
            } else {
                params[0] = exception.getSearchName();
                errorDetail = "{0} was not found";
            }

            errorTitle = "Record Not Found";
        }

        ErrorDto error = new ErrorDto(status.name(), String.valueOf(status.value()), errorTitle, MessageFormat.format(errorDetail, params), webRequest.getDescription(false));
        response.setErrors(Collections.singletonList(error));
        return response;
    }

    /**
     * createArgumentErrorObject
     * @param ex
     * @param webRequest
     * @return
     * @throws IOException
     */
    public CommonResponseDto createArgumentErrorObject(MethodArgumentNotValidException ex, WebRequest webRequest) throws IOException {
        CommonResponseDto response = new CommonResponseDto();
        List<FieldError> listSorted = ex.getBindingResult().getFieldErrors().stream().collect(Collectors.toList());
        Collections.sort(listSorted, Comparator.comparing((o) -> {
            return o.getCode();
        }));
        ModelMapper modelMapper = new ModelMapper();
        Iterator var6 = listSorted.iterator();

        while (var6.hasNext()) {
            FieldError fieldError = (FieldError) var6.next();
            response.addError(modelMapper.map(this.getErrorBadRequest(fieldError, webRequest), ErrorDto.class));
        }

        return response;
    }

    /**
     * createIllegalArgumentErrorObject
     * @param ex
     * @param webRequest
     * @return
     */
    public CommonResponseDto createIllegalArgumentErrorObject(Exception ex, WebRequest webRequest) {
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        String[] parameters = ex.getMessage().split(",");
        String[] var5 = parameters;
        int var6 = parameters.length;

        for (int var7 = 0; var7 < var6; ++var7) {
            String parameter = var5[var7];
            ErrorDto errorDto = Util.createErrorDto(HttpStatus.BAD_REQUEST, webRequest);
            errorDto.setTitle("Invalid data");
            errorDto.setDetail(MessageFormat.format("{0} parameter value is not valid", parameter.trim()));
            commonResponseDto.addError(errorDto);
        }

        return commonResponseDto;
    }





}