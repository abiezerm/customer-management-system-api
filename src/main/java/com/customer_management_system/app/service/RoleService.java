package com.customer_management_system.app.service;

import com.customer_management_system.app.entity.Role;
import com.customer_management_system.app.model.enums.RoleName;
import com.customer_management_system.app.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;


/**
 * Role Service.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since January 22, 2021
 * @see SpringApplication
 */
@Service
@Transactional
public class RoleService {

    /**
     * roleRepository
     */
    @Autowired
    RoleRepository roleRepository;

    /**
     * getByRoleName
     * @param roleName
     * @return
     */
    public Optional<Role> getByRoleName(RoleName roleName){
        return  roleRepository.findByRoleName(roleName);
    }

    /**
     * save
     * @param role
     */
    public void save(Role role){
        roleRepository.save(role);
    }
}