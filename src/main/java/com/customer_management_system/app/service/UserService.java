package com.customer_management_system.app.service;

import com.customer_management_system.app.entity.User;
import com.customer_management_system.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;


/**
 * User Service.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since January 22, 2021
 * @see SpringApplication
 */
@Service
@Transactional
public class UserService {

    /**
     * userRepository
     */
    @Autowired
    UserRepository userRepository;

    /**
     * getByUsuario
     * @param userName
     * @return
     */
    public Optional<User> getByUsuario(String userName){
        return userRepository.findByUserName(userName);
    }

    /**
     * existsByUser
     * @param userName
     * @return
     */
    public Boolean existsByUser(String userName){
        return userRepository.existsByUserName(userName);
    }

    /**
     * existsByEmail
     * @param email
     * @return
     */
    public Boolean existsByEmail(String email){
        return userRepository.existsByEmail(email);
    }

    /**
     * save
     * @param user
     */
    public void save(User user){
        userRepository.save(user);
    }


}