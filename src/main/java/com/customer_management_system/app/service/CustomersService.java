package com.customer_management_system.app.service;


import com.customer_management_system.app.entity.Customer;
import com.customer_management_system.app.exeption.BadRequestException;
import com.customer_management_system.app.exeption.RecordNotFoundException;
import com.customer_management_system.app.model.dto.CustomerDto;
import com.customer_management_system.app.model.enums.CustomerStatus;
import com.customer_management_system.app.repository.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * CustomersService class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Service
public class CustomersService {

    /**
     * This is the constant logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomersService.class);

    /**
     * Customer Repository bean
     */
    private final CustomerRepository customerRepository;

    /**
     * ModelMapper
     */
    final ModelMapper mapper = new ModelMapper();

    /**
     * bean Injection by Constructor
     *
     * @param customerRepository
     */
    public CustomersService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    /**
     * getAllItems method retrieve all the records form database
     *
     * @return Optional<List < ItemDto>> customer profile
     */
    public Optional<List<CustomerDto>> getAllCustomers() {

        List<Customer> itemList = customerRepository.findAll();

        itemList.stream().findFirst().orElseThrow(() -> new RecordNotFoundException("Records"));

        List<CustomerDto> dtoList=  itemList.stream()
                .map(item -> new ModelMapper().map(item, CustomerDto.class)).collect(Collectors.toList());


        LOGGER.info("Table items were retrieved");

        return Optional.ofNullable(dtoList);

    }

    /**
     * addCustomer method saved records in database
     *
     * @return Optional<List < ItemDto>> customer profile
     */
    public Optional<CustomerDto> addCustomers(CustomerDto customerDto) {

        Assert.notNull(customerDto, "itemDto must not be null.");
        final Boolean exists = customerRepository.existsBySocialSecurity(customerDto.getSocialSecurity());

        if (!exists) {
            LOGGER.info("Record alReady exits in database");
            throw new BadRequestException("Social security alReady exits in database");
        }

        customerDto.setCustomerEnteredDate(ZonedDateTime.now());
        customerDto.setCustomerStatus(CustomerStatus.ACTIVE.name());

        final Customer customer = this.mapper.map(customerDto, Customer.class);
        
        final Customer savedCustomer = customerRepository.save(customer);

        CustomerDto dto = this.mapper.map(savedCustomer, CustomerDto.class);
        LOGGER.info("Record was saved");

        return Optional.ofNullable(dto);

    }

    /**
     * updateCustomer method retrieve all the records form database
     *
     * @return Optional<CustomerDto> customer profile
     */
    public Optional<CustomerDto> updateCustomers(String status, String ssn) {

        Assert.notNull(status, "status must not be null.");
        Assert.notNull(ssn, "ssn must not be null.");

        Customer customer = customerRepository.findBySocialSecurity(ssn)
                .orElseThrow(() -> new RecordNotFoundException("customer does not exits"));

        customer.setCustomerStatus(status);

        Customer savedCustomer = customerRepository.save(customer);

        LOGGER.info("Record " + customer.getCustomerId() + " was updated");

        final CustomerDto dto = mapper.map(savedCustomer, CustomerDto.class);

        return Optional.ofNullable(dto);

    }


    /**
     * delete Customer By Ssn  from database
     *
     * @return Boolean
     */
    public Boolean deleteCustomerBySsn(String ssn) {

        Assert.notNull(ssn, "itemId must not be null.");

        final Integer deletedRecord = customerRepository.deleteCustomerBySocialSecurity(ssn);

        if (deletedRecord == 0) {
            LOGGER.info("Record not Found ");
            throw new RecordNotFoundException("Record " + ssn);
        }

        LOGGER.info("Record " + ssn + " was deleted");

        return Boolean.TRUE;

    }



    /**
     * get Customer By ssn records from database
     *
     * @return Optional<List < ItemDto>> customer profile
     */
    public Optional<CustomerDto> getCustomerBySsn(String ssn) {

        Assert.notNull(ssn, "ssn must not be null.");

         Customer customer = customerRepository.findBySocialSecurity(ssn).
                orElseThrow(() -> new RecordNotFoundException("customer"));;

         final CustomerDto customerDto = mapper.map(customer, CustomerDto.class);

        return Optional.ofNullable(customerDto);

    }



    /**
     * getAllItems method retrieve all the records form database
     *
     * @return Optional<List < ItemDto>> customer profile
     */
    public Optional<List<CustomerDto>> getCustomerPageAndSortByField(Pageable pageable) {

        Assert.notNull(pageable, "pageable must not be null.");

        final List<CustomerDto> customerPage = customerRepository.findAll(pageable).stream().map(data -> new ModelMapper().map(data, CustomerDto.class))
                .collect(Collectors.toList());

        customerPage.stream().findFirst().orElseThrow(() -> new RecordNotFoundException("Page"));

        return Optional.ofNullable(customerPage);

    }

}
