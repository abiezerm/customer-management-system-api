package com.customer_management_system.app.service;

import com.customer_management_system.app.entity.MainUser;
import com.customer_management_system.app.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


/**
 * UserDetailsServiceImpl.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since January 22, 2021
 * @see SpringApplication
 */
@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    /**
     * userService
     */
    @Autowired
    UserService userService;

    /**
     * loadUserByUsername
     * @param userName
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User usuario = userService.getByUsuario(userName).get();
        return MainUser.build(usuario);
    }
}