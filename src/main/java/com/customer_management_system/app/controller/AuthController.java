package com.customer_management_system.app.controller;

import com.customer_management_system.app.entity.Role;
import com.customer_management_system.app.entity.User;
import com.customer_management_system.app.exeption.BadRequestException;
import com.customer_management_system.app.model.dto.JwtDto;
import com.customer_management_system.app.model.dto.RequestUserDto;
import com.customer_management_system.app.model.dto.ServiceResponseDto;
import com.customer_management_system.app.model.dto.UserLoginDto;
import com.customer_management_system.app.model.enums.RoleName;
import com.customer_management_system.app.security.jwt.JwtProvider;
import com.customer_management_system.app.service.RoleService;
import com.customer_management_system.app.service.UserService;
import com.customer_management_system.app.util.Constants;
import com.customer_management_system.app.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

/**
 * Authentication Controller .
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {

    /**
     *  passwordEncoder
     */
    PasswordEncoder passwordEncoder;

    /**
     * authenticationManager
     */
    AuthenticationManager authenticationManager;

    /**
     * userService
     */
    UserService userService;

    /**
     * roleService
     */
    RoleService roleService;

    /**
     * jwtProvider
     */
    JwtProvider jwtProvider;

    /**
     * Injection by Contructor
     *
     * @param passwordEncoder
     * @param authenticationManager
     * @param userService
     * @param roleService
     * @param jwtProvider
     */
    @Autowired
    public AuthController(PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager, UserService userService, RoleService roleService, JwtProvider jwtProvider) {
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.roleService = roleService;
        this.jwtProvider = jwtProvider;
    }

    /**
     * sign-On Controller
     *
     * @param requestUserDto
     * @param bindingResult
     * @return
     */
    @PostMapping(value = Constants.ENDPOINT_SIGN_ON, produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceResponseDto> signOn(@Valid @RequestBody RequestUserDto requestUserDto,
                                                     BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            throw new BadRequestException("Invalid Field");
        }
        if(userService.existsByUser(requestUserDto.getUsername())){
            throw new BadRequestException("UserName alReady exist");
        }

        if(userService.existsByEmail(requestUserDto.getEmail())){
            throw new BadRequestException("email alReady exist");
        }

        User user = new User(requestUserDto.getCompanyName(), requestUserDto.getUsername(),
                requestUserDto.getEmail(), passwordEncoder.encode(requestUserDto.getPassword()));

        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getByRoleName(RoleName.ROLE_USER).get());
        if(requestUserDto.getRoles().contains("admin"))
            roles.add(roleService.getByRoleName(RoleName.ROLE_ADMIN).get());
        user.setRoles(roles);

        userService.save(user);

        return ResponseEntity.status(HttpStatus.CREATED).header(Constants.DATE_HEADER, new String[]{Util.getActualZoneDateTime()}).body(new ServiceResponseDto());
    }

    /**
     * log-in Controller, Used to get the user token
     * @param userLoginDto
     * @param bindingResult
     * @return
     */
    @PostMapping(Constants.ENDPOINT_LOGIN)
    public ResponseEntity<JwtDto> login(@Valid @RequestBody UserLoginDto userLoginDto, BindingResult bindingResult){

        if (bindingResult.hasErrors()) {
            throw new BadRequestException("User or Password incorrect");
        }
        Authentication authentication =
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(userLoginDto.getUserName(),
                                userLoginDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(), userDetails.getAuthorities());
        return new ResponseEntity<>(jwtDto, HttpStatus.OK);
    }
}