package com.customer_management_system.app.controller;

import com.customer_management_system.app.model.dto.CommonResponseDto;
import com.customer_management_system.app.model.dto.ErrorDto;
import com.customer_management_system.app.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Error Controller.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 24, 2021
 * @see SpringApplication
 */
@Controller
public class CustomBasicErrorController implements ErrorController {


    /**
     * logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomBasicErrorController.class);

    /**
     * ERROR_PATH
     */
    public static final String ERROR_PATH = "/error";


    /**
     * error handeler Controller
     * @param request
     * @return
     */
    @RequestMapping({ERROR_PATH})
    public ResponseEntity<CommonResponseDto> handleError(HttpServletRequest request) {
        LOGGER.info("Endpoint {} called", "/error");
        DefaultErrorAttributes defaultErrorAttr = new DefaultErrorAttributes();
        WebRequest webRequest = new ServletWebRequest(request);
        Map<String, Object> body = defaultErrorAttr.getErrorAttributes(webRequest, false);
        CommonResponseDto response = new CommonResponseDto();
        HttpStatus responseStatus = this.getStatus(request);
        if (responseStatus == HttpStatus.NO_CONTENT) {
            return (ResponseEntity.status(responseStatus).header("Date", new String[]{Util.getActualZoneDateTime()})).build();
        } else {
            response.addError(new ErrorDto(responseStatus.name(), String.valueOf(responseStatus.value()), (String)body.get("error"), (String)body.get("message"), (String)body.get("path")));
            return (ResponseEntity.status(responseStatus).header("Date", new String[]{Util.getActualZoneDateTime()})).body(response);
        }
    }

    /**
     * get http status
     * @param request
     * @return
     */
    private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer)request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } else {
            try {
                return HttpStatus.valueOf(statusCode);
            } catch (Exception var4) {
                return HttpStatus.INTERNAL_SERVER_ERROR;
            }
        }
    }

    /**
     * get ErrorPath
     * @return
     */
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
