package com.customer_management_system.app.entity;


import org.springframework.boot.SpringApplication;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Address Entity mapping.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Entity
@Table(name = "ADDRESS")
public class Address {


    /**
     * Unique id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ADDRESS_ID")
    private int addressId;

    /**
     * street name
     */
    @Column(name = "STREET")
    private String street;


    /**
     * apartment number
     */
    @Column(name = "APARTMENT")
    private String apartment;

    /**
     * state name
     */
    @Column(name = "STATE")
    private String state;

    /**
     * city name
     */
    @Column(name = "CITY")
    private String city;

    /**
     * zip code number
     */
    @Column(name = "ZIP_CODE")
    private Integer zipCode;


    /**
     * country name
     */
    @Column(name = "COUNTRY")
    private String country;


    /**
     * customer id
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID")
    private Customer customerId;


    /**
     * Gets city name.
     *
     * @return Value of city name.
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets new country name.
     *
     * @param country New value of country name.
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Sets new unique id.
     *
     * @param addressId New value of unique id.
     */
    public void setId(int addressId) {
        this.addressId = addressId;
    }

    /**
     * Gets customer id.
     *
     * @return Value of customer id.
     */
    public Customer getCustomerId() {
        return customerId;
    }

    /**
     * Gets country name.
     *
     * @return Value of country name.
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets new customer id.
     *
     * @param customerId New value of customer id.
     */
    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    /**
     * Sets new zip code number.
     *
     * @param zipCode New value of zip code number.
     */
    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * Gets zip code number.
     *
     * @return Value of zip code number.
     */
    public Integer getZipCode() {
        return zipCode;
    }

    /**
     * Sets new city name.
     *
     * @param city New value of city name.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Sets new state name.
     *
     * @param state New value of state name.
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Gets unique id.
     *
     * @return Value of unique id.
     */
    public int getAddressId() {
        return addressId;
    }

    /**
     * Gets state name.
     *
     * @return Value of state name.
     */
    public String getState() {
        return state;
    }

    /**
     * Gets apartment number.
     *
     * @return Value of apartment number.
     */
    public String getApartment() {
        return apartment;
    }

    /**
     * Sets new apartment number.
     *
     * @param apartment New value of apartment number.
     */
    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    /**
     * Sets new street name.
     *
     * @param street New value of street name.
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Gets street name.
     *
     * @return Value of street name.
     */
    public String getStreet() {
        return street;
    }
}
