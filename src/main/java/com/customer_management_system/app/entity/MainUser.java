package com.customer_management_system.app.entity;


import org.springframework.boot.SpringApplication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Authentication Controller .
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public class MainUser implements UserDetails {

    /**
     * companyName
     */
    private String companyName;

    /**
     * user
     */
    private String userName;

    /**
     * email
     */
    private String email;

    /**
     * password
     */
    private String password;

    /**
     * GrantedAuthority
     */
    private Collection<? extends GrantedAuthority> authorities;

    /**
     * Param Constructor
     * @param companyName
     * @param userName
     * @param email
     * @param password
     * @param authorities
     */
    public MainUser(String companyName, String userName, String email, String password, Collection<? extends GrantedAuthority> authorities) {
        this.companyName = companyName;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }

    /**
     * build MainUser
     * @param user
     * @return MainUser
     */
    public static MainUser build(User user){
        List<GrantedAuthority> authorities =
                user.getRoles()
                        .stream()
                        .map(rol -> new SimpleGrantedAuthority(rol.getRoleName().name()))
                        .collect(Collectors.toList());
        return new MainUser(user.getCompanyName(),user.getUserName(), user.getEmail(),
                user.getPassword(), authorities);
    }

    /**
     * GrantedAuthority
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * getPassword
     * @return password
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * getUsername
     * @return userName
     */
    @Override
    public String getUsername() {
        return userName;
    }

    /**
     * isAccountNonExpired
     * @return boolean
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * isAccountNonLocked
     * @return boolean
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     *  isCredentialsNonExpired
     * @return boolean
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     *  isEnabled
     * @return true
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

    /**
     *  getFirstName
     * @return  String
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * getEmail
     * @return String
     */
    public String getEmail() {
        return email;
    }
}