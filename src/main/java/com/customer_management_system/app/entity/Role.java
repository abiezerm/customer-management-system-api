package com.customer_management_system.app.entity;

import com.customer_management_system.app.model.enums.RoleName;
import org.springframework.boot.SpringApplication;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * Authentication Controller .
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
@Entity
@Table(name = "ROLE")
public class Role {


    /**
     * unique id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROLE_ID")
    private int id;

    /**
     * the role name
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE_NAME")
    private RoleName roleName;

    /**
     * defualt Constructor
     */
    public Role() {
    }

    /**
     * param Constructor
     * @param roleName
     */
    public Role(@NotNull RoleName roleName) {
        this.roleName = roleName;
    }


    /**
     * Gets the role name.
     *
     * @return Value of the role name.
     */
    public RoleName getRoleName() {
        return roleName;
    }

    /**
     * Sets new unique id.
     *
     * @param id New value of unique id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets unique id.
     *
     * @return Value of unique id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets new the role name.
     *
     * @param roleName New value of the role name.
     */
    public void setRoleName(RoleName roleName) {
        this.roleName = roleName;
    }
}
