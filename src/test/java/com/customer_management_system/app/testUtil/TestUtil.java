package com.customer_management_system.app.testUtil;


import com.customer_management_system.app.entity.Customer;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * CustomersService class.
 *
 * @author Richie Martinez
 * @Version 1.1
 * @Since March 23, 2021
 * @see SpringApplication
 */
public class TestUtil {

    public  static final String JSON="{\n" +
            "\"firstName\":\"Raul\",\n" +
            "\"lastName\": \"Mendes\",\n" +
            "\"socialSecurity\": \"852147963\",\n" +
            "\"customerAddress\":[\n" +
            "    {\n" +
            "\"street\":\"Sprint\",\n" +
            "\"apartment\":\"B4\",\n" +
            "\"state\": \"NY\",\n" +
            "\"city\": \"NY\",\n" +
            "\"zipCode\": \"12345\",\n" +
            "\"country\": \"US\"\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "]\n" +
            "\n" +
            "}";

    /**
     * List<Customer>
     * @return List<Customer>
     */
    public static List<Customer> getCustomerList(){
        List<Customer> list= new ArrayList<>();

        IntStream.range(1,5).forEach(value -> list.add(new Customer()));

        return list;

    }
}
