package com.customer_management_system.app.controller;

import com.customer_management_system.app.exeption.RestResponseEntityExceptionHandler;
import com.customer_management_system.app.model.dto.CustomerDto;
import com.customer_management_system.app.service.CustomersService;
import com.customer_management_system.app.testUtil.TestUtil;
import com.customer_management_system.app.util.Constants;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Create a unit testing to check customBasicErrorController.
 *
 * @author Richie Martinez
 * @version 1.0
 * @since March 24, 2020
 */
@ExtendWith(MockitoExtension.class)
class CustomerControllerUT {


    /**
     * The mock mvc
     */
    private MockMvc mockMvc;

    /**
     * The customer controller
     */
    @InjectMocks
    private CustomerController customerController;

    /**
     * The mock Customer Service
     */
    @Mock
    private CustomersService customersService;


    /**
     * Setup
     */
    @BeforeEach
    public void setup() {

        this.mockMvc = MockMvcBuilders.standaloneSetup(this.customerController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler()).build();

    }

    /**
     * When the client invoke the method GetCustomer Given a good request Then
     * response OK
     *
     * @throws Exception the exception
     */
    @Test
    public void WhenGetCustomer_IsInvoked_ThenReturnHttpStatusCodeOk() throws Exception {

        doReturn(Optional.ofNullable(new ArrayList())).when(this.customersService).getAllCustomers();

        mockMvc.perform(get(Constants.PATH.concat(Constants.ENDPOINT_CUSTOMERS))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());


    }


    /**
     * When the client invoke the method getCustomerBySsn( Given a good request Then
     * response OK
     *
     * @throws Exception the exception
     */
    @Test
    public void WhenGetCustomerBySsn_IsInvoked_ThenReturnHttpStatusCodeOk() throws Exception {

        doReturn(Optional.ofNullable(new CustomerDto())).when(this.customersService).getCustomerBySsn(any());

        String param = "?ssn=852369741";
        mockMvc.perform(get(Constants.PATH.concat(Constants.ENDPOINT_CUSTOMERS.concat(param)))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());


    }


    /**
     * When the client invoke the method DeleteCustomerBySsn Given a good request Then
     * response OK
     *
     * @throws Exception the exception
     */
    @Test
    public void WhenDeleteCustomerBySsn_IsInvoked_ThenReturnHttpStatusCodeOk() throws Exception {

        doReturn(Boolean.TRUE).when(this.customersService).deleteCustomerBySsn(any());

        String param = "?ssn=852369741";

        mockMvc.perform(delete(Constants.PATH.concat(Constants.ENDPOINT_CUSTOMERS.concat(param)))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());


    }


    /**
     * When the client invoke the method AddCustomers Given a good request Then
     * response Created
     *
     * @throws Exception the exception
     */
    @Test
    public void WhenAddCustomers_IsInvoked_ThenReturnHttpStatusCodeCreated() throws Exception {

        when(this.customersService.addCustomers(any())).thenReturn(Optional.ofNullable(new CustomerDto()));

        final String request = TestUtil.JSON;

        mockMvc.perform(post(Constants.PATH.concat(Constants.ENDPOINT_CUSTOMERS))
                .content(request)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated());

    }


}