package com.customer_management_system.app.controller;


import com.customer_management_system.app.exeption.RestResponseEntityExceptionHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


/**
 * Create a unit testing to check customBasicErrorController.
 *
 * @author Richie Martinez
 * @version 1.0
 * @since March 24, 2020
 */
@ExtendWith(MockitoExtension.class)
public class CustomBasicErrorControllerUT {

    /**
     * Mock mvc
     */
    private MockMvc mockMvc;

    /**
     * Controller to test
     */
    @InjectMocks
    private CustomBasicErrorController customBasicErrorController;

    /**
     * Initial setup
     */
    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(customBasicErrorController).
                setControllerAdvice(RestResponseEntityExceptionHandler.class).build();
    }

    /**
     * Check error path to see is working as expected.
     */
    @Test
    public void WhenhandleErrorIsInvoked_GivenACallToErrorPath_ThenReturnInternalServerError() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/error")).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }


}
